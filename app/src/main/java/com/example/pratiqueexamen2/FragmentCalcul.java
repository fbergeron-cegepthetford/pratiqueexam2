package com.example.pratiqueexamen2;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pratiqueexamen2.affaire.Calculateur;
import com.example.pratiqueexamen2.databinding.FragmentCalculBinding;


public class FragmentCalcul extends Fragment {

    FragmentCalculBinding binding;
    Context cx;

    Calculateur calc;

    public FragmentCalcul() {
    }

    @Override
    public View onCreateView (LayoutInflater inflater,
                              ViewGroup container,
                              Bundle savedInstanceState) {
        binding = FragmentCalculBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        cx = getApplicationContext();
        initialiserModele();
        initialiserVue();
    }

    private void initialiserVue() {
        binding.btnCalculer.setOnClickListener(view -> {
            float brut = Float.parseFloat(binding.etBrut.getText().toString());
            try {
                calc.setMontant(brut);
                afficherValeurs();
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(cx, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void afficherValeurs() {
        binding.tvMontantBrut.setText(String.format("%.2f", calc.getMontant()));
        binding.tvTPS.setText(String.format("%.2f", calc.calculerTaxeFederale()));
        binding.tvTVQ.setText(String.format("%.2f", calc.calculerTaxeProvinciale()));
        binding.tvPourboire.setText(String.format("%.2f", calc.calculerPourboire()));
        binding.tvMontantTotal.setText(String.format("%.2f", calc.calculerMontantTotal()));
    }

    private void initialiserModele() {
        calc = new Calculateur();
    }
}